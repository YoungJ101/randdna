#include <string>
#include <random>
using std::string;
using namespace std;

string randDNA(int s, string b, int n)
{
	string ATCG;
	std::mt19937 eng1(s);
	uniform_int_distribution<int> un(0, b.size()-1);
	
	if (b.size() == 0) {
	ATCG = "";
	 return ATCG;
 }
 
	for (int i=0; i<n; i++) {
		 ATCG += b[un(eng1)];
	}
	
	return ATCG;
	
}

